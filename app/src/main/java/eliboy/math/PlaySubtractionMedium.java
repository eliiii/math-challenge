package eliboy.math;


public class PlaySubtractionMedium extends PlaySubtraction {


    @Override
    public int createLeftMember() {
        return random.nextInt(40) + 21;
    }

    @Override
    public int createRightMember() {
        do {
            return random.nextInt(30) + 1;
        } while (left_number < right_number);
    }

    @Override
    public void setBcBounty() {
        bcBounty = 3;
    }
}
