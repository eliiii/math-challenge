package eliboy.math;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;


public abstract class PlaySuperClassBasicOperations extends ZCoreFunctions {


    private View layoutExerciseArea;
    private int animDelayTime = 0;
    private int animListAppear3;
    private int animListDisappear3;

    private TextView left_member;
    private TextView right_member;
    private Button button_1;
    private Button button_2;
    private Button button_3;
    int correct_button;
    int error_1;
    int error_2;
    int left_number;
    int right_number;
    int result;
    int bcBounty;


    final Random random = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setLayout();


        left_member = (TextView) findViewById(R.id.left_member);
        right_member = (TextView) findViewById(R.id.right_member);

        button_1 = (Button) findViewById(R.id.button_1);
        button_2 = (Button) findViewById(R.id.button_2);
        button_3 = (Button) findViewById(R.id.button_3);


        switch (settingsGetAnimationType()) {
            case 1:
                animListAppear3 = R.anim.horizontal_list_appear_3;
                animListDisappear3 = R.anim.horizontal_list_disappear_3;
                animDelayTime = 500;
                break;
            case 2:
                animListAppear3 = R.anim.vertical_list_appear_3;
                animListDisappear3 = R.anim.vertical_list_disappear_3;
                animDelayTime = 500;
                break;
            case 0:
                animListAppear3 = R.anim.empty;
                animListDisappear3 = R.anim.empty;
                animDelayTime = 300;
                break;
        }

        layoutExerciseArea = findViewById(R.id.layout_exercise_area);


        refresh();
        setBcBounty();
    }

	
	/*
     * This method refresh the values giving a
	 * new exercise every time a user finish one
	 * */

    void refresh() {

        left_number = createLeftMember();
        right_number = createRightMember();
        result = createResult();

        correct_button = createRandomButton();


        createErrors();
        printValueMembers();
        printValueButtons();

    }
	
	/*
	 * Checks what button was pressed with a switch statement,
	 *  then display a toast
	 */


    public void checkAnswer(View v) {
        switch (v.getId()) {
            case R.id.button_1:
                if (correct_button == 1) {
                    bcAddBc(bcBounty);
                } else {
                    bcRemoveBc(bcBounty);
                }
                break;
            case R.id.button_2:
                if (correct_button == 2) {
                    bcAddBc(bcBounty);
                } else {
                    bcRemoveBc(bcBounty);
                }
                break;
            case R.id.button_3:
                if (correct_button == 3) {
                    bcAddBc(bcBounty);
                } else {
                    bcRemoveBc(bcBounty);
                }
                break;
        }


        button_1.setBackgroundResource(R.drawable.button_incorrect);
        button_2.setBackgroundResource(R.drawable.button_incorrect);
        button_3.setBackgroundResource(R.drawable.button_incorrect);

        switch (correct_button) {
            case 1:
                button_1.setBackgroundResource(R.drawable.button_correct);
                break;
            case 2:
                button_2.setBackgroundResource(R.drawable.button_correct);
                break;
            case 3:
                button_3.setBackgroundResource(R.drawable.button_correct);
                break;
        }

        gameAnimateOUT(layoutExerciseArea, animListDisappear3);
        new Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        button_1.setBackgroundResource(R.drawable.button_neutral);
                        button_2.setBackgroundResource(R.drawable.button_neutral);
                        button_3.setBackgroundResource(R.drawable.button_neutral);
                        refresh();
                        gameAnimateIN(layoutExerciseArea, animListAppear3);
                    }
                }, animDelayTime);

    }
	
	
	/*
	 * Choose layout
	 */

    void setLayout() {

    }

    /*
     * Create members
     */
    int createLeftMember() {
        return 0;
    }

    int createRightMember() {
        return 0;
    }
	
	/*
	 * Result;
	 * Should be overridden by game specific super-class
	 */

    int createResult() {
        return 0;
    }


    /*
     * Random correct button
     * Good for all
     */
    int createRandomButton() {
        return random.nextInt(3) + 1;
    }
	
	
	/*
	 * Toast :)
	 * Good for all
	 */


    /*
     * print the value of members
     */
    void printValueMembers() {
        left_member.setText(Integer.toString(left_number));
        right_member.setText(Integer.toString(right_number));
    }

    /*
     * print answers to buttons
     */
    void printValueButtons() {
        switch (correct_button) {
            case 1:
                button_1.setText(Integer.toString(result));
                button_2.setText(Integer.toString(error_1));
                button_3.setText(Integer.toString(error_2));
                break;
            case 2:
                button_2.setText(Integer.toString(result));
                button_1.setText(Integer.toString(error_1));
                button_3.setText(Integer.toString(error_2));
                break;
            case 3:
                button_3.setText(Integer.toString(result));
                button_1.setText(Integer.toString(error_1));
                button_2.setText(Integer.toString(error_2));
                break;
        }
    }
	
	
	/*
	 * Double do while functions.
	 * They take care of equality between them
	 * and with zero
	 */

    void createErrors() {
        do {
            error_1 = result + random.nextInt(11) - 5;
        }
        while (error_1 == 0 || error_1 <= 1 || error_1 == result);

        do {
            error_2 = result + random.nextInt(11) - 5;
        }
        while (error_2 == 0 || error_2 == error_1 || error_2 <= 1 || error_2 == result);
    }

    /*
     *
     * OP method for setting bc reward value :)
     * default is 1	 */
    void setBcBounty() {
        bcBounty = 1;
    }


}
