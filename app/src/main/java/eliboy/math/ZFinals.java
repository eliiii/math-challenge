package eliboy.math;


class ZFinals {
    /*
     * This is the name of the shared preferences file
     */
    public final static String SHARED_PREFS_NAME = "easySocial";

    /*
     * Stuff related to settings
     */
    public final static String SETTINGS_ANIMATION = "animation_type";
    public final static String SETTINGS_GAME_MODE = "game_mode";

    /*
     * Things related to scores
     */
    public final static String SCORES_TOTAL_BC = "bc";

    /*
     * Some default values
     */
    public final static int DEFAULT_ANIMATION_TYPE = 1; // 2 - vertical animations

}
