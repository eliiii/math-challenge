package eliboy.math;


public class PlaySubtractionHard extends PlaySubtraction {


    @Override
    public int createLeftMember() {
        return random.nextInt(105) + 46;
    }

    @Override
    public int createRightMember() {
        return random.nextInt(140) + 11;
    }

    @Override
    public void createErrors() {
        do {
            error_1 = result + random.nextInt(11) - 5;
        }
        while (error_1 == 0 || error_1 == result);

        do {
            error_2 = result + random.nextInt(11) - 5;
        }
        while (error_2 == 0 || error_2 == error_1 || error_2 == result);
    }

    @Override
    public void setBcBounty() {
        bcBounty = 4;
    }

}
