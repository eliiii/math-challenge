package eliboy.math;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;


public class PlayPrimeNumbers extends ZCoreFunctions {


    private Button buttonPrime;
    private Button buttonNotPrime;

    private int animDelayTime = 0;
    private int animListAppear3;
    private int animListDisappear3;

    private int number;
    private boolean isPrime = false;
    private TextView textViewNumber;
    private final int[] primeNumbers = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41,
            43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109};


    private int correctAnswers = 0; // track high score


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_prime_numbers);

        buttonPrime = (Button) findViewById(R.id.button_1);
        buttonNotPrime = (Button) findViewById(R.id.button_2);


        switch (settingsGetAnimationType()) {
            case 1:
                animListAppear3 = R.anim.horizontal_list_appear_3;
                animListDisappear3 = R.anim.horizontal_list_disappear_3;
                animDelayTime = 500;
                break;
            case 2:
                animListAppear3 = R.anim.vertical_list_appear_3;
                animListDisappear3 = R.anim.vertical_list_disappear_3;
                animDelayTime = 500;
                break;
            case 0:
                animListAppear3 = R.anim.empty;
                animListDisappear3 = R.anim.empty;
                animDelayTime = 200;
                break;
        }

        textViewNumber = (TextView) findViewById(R.id.layout_exercise_area);

        refresh();
    }


    private void refresh() {
        isPrime = false;
        Random random = new Random();
        number = random.nextInt(109) + 2;

        for (int primeNumber : primeNumbers) {
            if (primeNumber == number) {
                isPrime = true;
            }
        }


        textViewNumber.setText(Integer.toString(number));

    }


    public void checkAnswer(View v) {
        switch (v.getId()) {
            case R.id.button_1:
                if (isPrime) {
                    bcAddBc(2);
                } else {
                    bcRemoveBc(2);
                }
                break;
            case R.id.button_2:
                if (!isPrime) {
                    bcAddBc(2);
                    correctAnswers++;
                } else {
                    bcRemoveBc(2);
                }
                break;
        }


        if (isPrime) {
            buttonPrime.setBackgroundResource(R.drawable.button_correct);
            buttonNotPrime.setBackgroundResource(R.drawable.button_incorrect);
        } else {
            buttonPrime.setBackgroundResource(R.drawable.button_incorrect);
            buttonNotPrime.setBackgroundResource(R.drawable.button_correct);
        }
        gameAnimateOUT(textViewNumber, animListDisappear3);
        new Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        buttonPrime.setBackgroundResource(R.drawable.button_neutral);
                        buttonNotPrime.setBackgroundResource(R.drawable.button_neutral);
                        refresh();
                        gameAnimateIN(textViewNumber, animListAppear3);
                    }
                }, animDelayTime);
    }


}
