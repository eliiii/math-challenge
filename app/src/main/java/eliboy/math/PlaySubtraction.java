package eliboy.math;

public abstract class PlaySubtraction extends PlaySuperClassBasicOperations {

    @Override
    public void setLayout() {
        setContentView(R.layout.play_subtraction);
    }

    @Override
    public int createResult() {
        return left_number - right_number;
    }


}

	

