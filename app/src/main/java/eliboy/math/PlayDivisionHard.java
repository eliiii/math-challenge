package eliboy.math;


public class PlayDivisionHard extends PlayDivision {


    @Override
    public int createResult() {
        return random.nextInt(40) + 21;
    }

    @Override
    public int createRightMember() {
        return random.nextInt(50) + 11;
    }

    @Override
    public int createLeftMember() {
        return right_number * result;
    }

    @Override
    public void setBcBounty() {
        bcBounty = 6;
    }

}
