package eliboy.math;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;


public class Main extends ZCoreFunctions {

    /*
     * Intent
     * we need this to launch the games
     */
    private Intent intent;
    /*
     * Intent
	 */

    // Top text score
    private TextView textViewScore;

    // base layout text views
    private TextView textViewPlay;
    private TextView textViewSettings;

    // algebra layout text views
    private TextView textViewEvenOdd;
    private TextView textViewPrimeNumbers;
    private TextView textViewAddition;
    private TextView textViewSubtraction;
    private TextView textViewMultiplication;
    private TextView textViewDivision;
    private TextView textViewPowers;

    //layouts
    private View layoutBase;
    private View layoutAlgebra;
    private View layoutBc;

    // Difficulty layouts
    private View layoutDifficultyAddition;
    private View layoutDifficultySubtraction;
    private View layoutDifficultyMultiplication;
    private View layoutDifficultyDivision;
    private View layoutDifficultyPowers;

    private int layoutLevel = 0;

    private boolean difficultyIsShown = false;

    //animations :)
    private int animDelayTime;

    private int animListAppear1;
    private int animListAppear2;
    private int animListAppear3;
    private int animListAppear4;
    private int animListAppear5;
    private int animListAppear6;
    private int animListAppear7;

    private int animListAppearReverse3;
    private int animListAppearReverse6;

    private int animListDisappear1;
    private int animListDisappear2;
    private int animListDisappear3;
    private int animListDisappear4;
    private int animListDisappear5;
    private int animListDisappear6;
    private int animListDisappear7;

    private int animFadeIn;
    private int animFadeOut;

    private int animDefaultPlay;
    private int animDefaultOthers;

    private int animSpecialListAppear3; //only for vertical

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        updateAnimations();
        updateLayouts();
        animateViews();
    }

    private void animateViews() {
        animationHelper(textViewPlay, animDefaultPlay);
        animationHelper(textViewSettings, animDefaultOthers);
        animationHelper(layoutBc, animDefaultOthers);
    }

    private void updateLayouts() {
        updateBaseLayouts();
        updateDifficultyLayouts();
        updateTextViews();
    }

    private void updateTextViews() {
        textViewScore = (TextView) findViewById(R.id.textview_score);

        textViewPlay = (TextView) findViewById(R.id.textview_play);
        textViewSettings = (TextView) findViewById(R.id.textview_settings);

        textViewEvenOdd = (TextView) findViewById(R.id.textview_even_odd);
        textViewPrimeNumbers = (TextView) findViewById(R.id.textview_prime_numbers);
        textViewAddition = (TextView) findViewById(R.id.textview_addition);
        textViewSubtraction = (TextView) findViewById(R.id.textview_subtraction);
        textViewMultiplication = (TextView) findViewById(R.id.textview_multiplication);
        textViewDivision = (TextView) findViewById(R.id.textview_division);
        textViewPowers = (TextView) findViewById(R.id.textview_powers);
    }

    private void updateDifficultyLayouts() {
        layoutDifficultyAddition = findViewById(R.id.layout_difficulty_addition);
        layoutDifficultySubtraction = findViewById(R.id.layout_difficulty_subtraction);
        layoutDifficultyMultiplication = findViewById(R.id.layout_difficulty_multiplication);
        layoutDifficultyDivision = findViewById(R.id.layout_difficulty_division);
        layoutDifficultyPowers = findViewById(R.id.layout_difficulty_powers);
    }

    private void updateBaseLayouts() {
        layoutBase = findViewById(R.id.layout_base);
        layoutAlgebra = findViewById(R.id.layout_algebra);
        layoutBc = findViewById(R.id.layout_bc);
    }

    private void updateAnimations() {
        switch (settingsGetAnimationType()) {
            case 1:
                setHorizontalAnimations();
                break;
            case 2:
                setVerticalAnimations();
                break;
            case 0:
                setNoAnimations();
                break;
        }
    }

    private void setNoAnimations() {
        animListAppear1 = R.anim.empty;
        animListAppear2 = R.anim.empty;
        animListAppear3 = R.anim.empty;
        animListAppear4 = R.anim.empty;
        animListAppear5 = R.anim.empty;
        animListAppear6 = R.anim.empty;
        animListAppear7 = R.anim.empty;

        animListAppearReverse3 = R.anim.empty;
        animListAppearReverse6 = R.anim.empty;

        animListDisappear1 = R.anim.empty;
        animListDisappear2 = R.anim.empty;
        animListDisappear3 = R.anim.empty;
        animListDisappear4 = R.anim.empty;
        animListDisappear5 = R.anim.empty;
        animListDisappear6 = R.anim.empty;
        animListDisappear7 = R.anim.empty;

        animFadeIn = R.anim.empty;
        animFadeOut = R.anim.empty;

        animDefaultPlay = R.anim.empty;
        animDefaultOthers = R.anim.empty;

        animSpecialListAppear3 = R.anim.empty;

        animDelayTime = 0;
    }

    private void setVerticalAnimations() {
        animListAppear1 = R.anim.vertical_list_appear_1;
        animListAppear2 = R.anim.vertical_list_appear_2;
        animListAppear3 = R.anim.vertical_list_appear_3;
        animListAppear4 = R.anim.vertical_list_appear_4;
        animListAppear5 = R.anim.vertical_list_appear_5;
        animListAppear6 = R.anim.vertical_list_appear_6;
        animListAppear7 = R.anim.vertical_list_appear_7;

        animListAppearReverse3 = R.anim.vertical_list_appear_up_3;
        animListAppearReverse6 = R.anim.vertical_list_appear_up_6;

        animListDisappear1 = R.anim.vertical_list_disappear_1;
        animListDisappear2 = R.anim.vertical_list_disappear_2;
        animListDisappear3 = R.anim.vertical_list_disappear_3;
        animListDisappear4 = R.anim.vertical_list_disappear_4;
        animListDisappear5 = R.anim.vertical_list_disappear_5;
        animListDisappear6 = R.anim.vertical_list_disappear_6;
        animListDisappear7 = R.anim.vertical_list_disappear_7;

        animFadeIn = R.anim.fade_in;
        animFadeOut = R.anim.fade_out;

        animDefaultPlay = R.anim.default_first_play;
        animDefaultOthers = R.anim.default_first_other;

        animSpecialListAppear3 = R.anim.vertical_list_appear_special_3;


        animDelayTime = 250;
    }

    private void setHorizontalAnimations() {
        animListAppear1 = R.anim.horizontal_list_appear_1;
        animListAppear2 = R.anim.horizontal_list_appear_2;
        animListAppear3 = R.anim.horizontal_list_appear_3;
        animListAppear4 = R.anim.horizontal_list_appear_4;
        animListAppear5 = R.anim.horizontal_list_appear_5;
        animListAppear6 = R.anim.horizontal_list_appear_6;
        animListAppear7 = R.anim.horizontal_list_appear_7;

        animListAppearReverse3 = R.anim.horizontal_list_appear_right_3;
        animListAppearReverse6 = R.anim.horizontal_list_appear_right_6;

        animListDisappear1 = R.anim.horizontal_list_disappear_1;
        animListDisappear2 = R.anim.horizontal_list_disappear_2;
        animListDisappear3 = R.anim.horizontal_list_disappear_3;
        animListDisappear4 = R.anim.horizontal_list_disappear_4;
        animListDisappear5 = R.anim.horizontal_list_disappear_5;
        animListDisappear6 = R.anim.horizontal_list_disappear_6;
        animListDisappear7 = R.anim.horizontal_list_disappear_7;

        animFadeIn = R.anim.fade_in;
        animFadeOut = R.anim.fade_out;

        animDefaultPlay = R.anim.default_first_play;
        animDefaultOthers = R.anim.default_first_other;

        animSpecialListAppear3 = animListAppear3;

        animDelayTime = 250;
    }

    @Override
    public void onResume() {
        super.onResume();
        textViewScore.setText("" + bcGetBc());
    }

    @Override
    public void onBackPressed() {
        if (layoutLevel == 0) {
            this.finish();
        } else if (layoutLevel == 11
                || layoutLevel == 12
                || layoutLevel == 13
                || layoutLevel == 14
                || layoutLevel == 15) {
            difficultyChooserCloseAll();
        } else if (layoutLevel == 1000) { // we wait for geometry
            animationHelper(textViewEvenOdd, animListDisappear1);
            animationHelper(textViewPrimeNumbers, animListDisappear2);
            animationHelper(textViewAddition, animListDisappear3);
            animationHelper(textViewSubtraction, animListDisappear4);
            animationHelper(textViewMultiplication, animListDisappear5);
            animationHelper(textViewDivision, animListDisappear6);
            animationHelper(textViewPowers, animListDisappear7);

            new Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            layoutAlgebra.setVisibility(View.GONE);
                            layoutBase.setVisibility(View.VISIBLE);
                            textViewSettings.setVisibility(View.VISIBLE);
                        }
                    }, animDelayTime);

            animationHelper(textViewPlay, animListAppearReverse3);
            animationHelper(textViewSettings, animFadeIn);

            layoutLevel = 0;
        }
    }

    /*
     * Launch Activity
     * launch various activities based on id
     */
    public void launchActivity(View v) {
        switch (v.getId()) {
            case R.id.textview_settings:
                intent = new Intent(this, Settings.class);
                startActivity(intent);
                this.finish();
                break;
        }
    }

    /*
     * method used till geometry is complete
     */
    public void onPlayClicked(View v) {
        animationHelper(textViewPlay, animListDisappear3);
        animationHelper(textViewSettings, animFadeOut);

        new Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        layoutBase.setVisibility(View.GONE);
                        layoutAlgebra.setVisibility(View.VISIBLE);
                        textViewSettings.setVisibility(View.GONE);
                    }
                }, animDelayTime);

        animationHelper(textViewEvenOdd, animListAppear1);
        animationHelper(textViewPrimeNumbers, animListAppear2);
        animationHelper(textViewAddition, animListAppear3);
        animationHelper(textViewSubtraction, animListAppear4);
        animationHelper(textViewMultiplication, animListAppear5);
        animationHelper(textViewDivision, animListAppear6);
        animationHelper(textViewPowers, animListAppear7);

        layoutLevel = 1000;
    }

    /*
     * Algebra difficulty chooser
     */
    public void difficultyChooserAlgebra(View v) {
        switch (v.getId()) {
            case R.id.textview_even_odd: // there is no difficulty level for this so we launch the activity
                intent = new Intent(Main.this, PlayEvenOdd.class);
                startActivity(intent);
                break;
            case R.id.textview_prime_numbers: // there is no difficulty level for this so we launch the activity
                intent = new Intent(Main.this, PlayPrimeNumbers.class);
                startActivity(intent);
                break;
            case R.id.textview_addition: // show the available difficulties
                if (layoutLevel != 11) {
                    if (difficultyIsShown) {
                        difficultyChooserCloseAll();
                        difficultyChooserDelayedShower(layoutDifficultyAddition);
                    } else {
                        layoutDifficultyAddition.setVisibility(View.VISIBLE);
                        animationHelper(layoutDifficultyAddition, animSpecialListAppear3);
                    }
                    layoutLevel = 11;
                    difficultyIsShown = true;
                } else difficultyChooserCloseAll();
                break;
            case R.id.textview_subtraction: // show the available difficulties
                if (layoutLevel != 12) {// do nothing if is already shown
                    if (difficultyIsShown) {
                        difficultyChooserCloseAll();
                        difficultyChooserDelayedShower(layoutDifficultySubtraction);
                    } else {
                        layoutDifficultySubtraction.setVisibility(View.VISIBLE);
                        animationHelper(layoutDifficultySubtraction, animSpecialListAppear3);
                    }
                    layoutLevel = 12;
                    difficultyIsShown = true;
                } else difficultyChooserCloseAll();
                break;
            case R.id.textview_multiplication: // show the available difficulties
                if (layoutLevel != 13) { // do nothing if is already shown
                    if (difficultyIsShown) {
                        difficultyChooserCloseAll();
                        difficultyChooserDelayedShower(layoutDifficultyMultiplication);
                    } else {
                        layoutDifficultyMultiplication.setVisibility(View.VISIBLE);
                        animationHelper(layoutDifficultyMultiplication, animSpecialListAppear3);
                    }
                    layoutLevel = 13;
                    difficultyIsShown = true;
                } else difficultyChooserCloseAll();
                break;
            case R.id.textview_division: // show the available difficulties
                if (layoutLevel != 14) { // do nothing if is already shown
                    if (difficultyIsShown) {
                        difficultyChooserCloseAll();
                        difficultyChooserDelayedShower(layoutDifficultyDivision);
                    } else {
                        layoutDifficultyDivision.setVisibility(View.VISIBLE);
                        animationHelper(layoutDifficultyDivision, animSpecialListAppear3);
                    }
                    layoutLevel = 14;
                    difficultyIsShown = true;
                } else difficultyChooserCloseAll();
                break;
            case R.id.textview_powers: // show the available difficulties
                if (layoutLevel != 15) { // do nothing if is already shown
                    if (difficultyIsShown) {
                        difficultyChooserCloseAll();
                        difficultyChooserDelayedShower(layoutDifficultyPowers);
                    } else {
                        layoutDifficultyPowers.setVisibility(View.VISIBLE);
                        animationHelper(layoutDifficultyPowers, animSpecialListAppear3);
                    }
                    layoutLevel = 15;
                    difficultyIsShown = true;
                } else difficultyChooserCloseAll();
                break;
        }
    }

    private void difficultyChooserCloseAll() {
        switch (layoutLevel) {
            case 11:
                animationHelper(layoutDifficultyAddition, animListDisappear3);
                difficultyChooserDelayedHider(layoutDifficultyAddition);
                break;
            case 12:
                animationHelper(layoutDifficultySubtraction, animListDisappear3);
                difficultyChooserDelayedHider(layoutDifficultySubtraction);
                break;
            case 13:
                animationHelper(layoutDifficultyMultiplication, animListDisappear3);
                difficultyChooserDelayedHider(layoutDifficultyMultiplication);
                break;
            case 14:
                animationHelper(layoutDifficultyDivision, animListDisappear3);
                difficultyChooserDelayedHider(layoutDifficultyDivision);
                break;
            case 15:
                animationHelper(layoutDifficultyPowers, animListDisappear3);
                difficultyChooserDelayedHider(layoutDifficultyPowers);
                break;
        }

        difficultyIsShown = false;
        layoutLevel = 1000;
    }

    /*
     * start activities
     */
    public void difficultyChooserEasy(View v) {
        switch (layoutLevel) {
            case 11:
                intent = new Intent(Main.this, PlayAdditionEasy.class);
                startActivity(intent);
                break;
            case 12:
                intent = new Intent(Main.this, PlaySubtractionEasy.class);
                startActivity(intent);
                break;
            case 13:
                intent = new Intent(Main.this, PlayMultiplicationEasy.class);
                startActivity(intent);
                break;
            case 14:
                intent = new Intent(Main.this, PlayDivisionEasy.class);
                startActivity(intent);
                break;
            case 15:
                intent = new Intent(Main.this, PlayPowersEasy.class);
                startActivity(intent);
                break;
        }
    }

    public void difficultyChooserMedium(View v) {
        switch (layoutLevel) {
            case 11:
                intent = new Intent(Main.this, PlayAdditionMedium.class);
                startActivity(intent);
                break;
            case 12:
                intent = new Intent(Main.this, PlaySubtractionMedium.class);
                startActivity(intent);
                break;
            case 13:
                intent = new Intent(Main.this, PlayMultiplicationMedium.class);
                startActivity(intent);
                break;
            case 14:
                intent = new Intent(Main.this, PlayDivisionMedium.class);
                startActivity(intent);
                break;
            case 15:
                intent = new Intent(Main.this, PlayPowersMedium.class);
                startActivity(intent);
                break;
        }
    }

    public void difficultyChooserHard(View v) {
        switch (layoutLevel) {
            case 11:
                intent = new Intent(Main.this, PlayAdditionHard.class);
                startActivity(intent);
                break;
            case 12:
                intent = new Intent(Main.this, PlaySubtractionHard.class);
                startActivity(intent);
                break;
            case 13:
                intent = new Intent(Main.this, PlayMultiplicationHard.class);
                startActivity(intent);
                break;
            case 14:
                intent = new Intent(Main.this, PlayDivisionHard.class);
                startActivity(intent);
                break;
        }
    }

    /*
     * delayed runnable hider
     */
    private void difficultyChooserDelayedHider(final View v) {
        new Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        v.setVisibility(View.GONE);

                    }
                }, animDelayTime);
    }

    private void difficultyChooserDelayedShower(final View v) {
        new Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        v.setVisibility(View.VISIBLE);
                        animationHelper(v, animSpecialListAppear3);

                    }
                }, animDelayTime);
    }
}
