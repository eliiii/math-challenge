package eliboy.math;


public class PlayAdditionHard extends PlayAddition {

    @Override
    public int createLeftMember() {
        return random.nextInt(70) + 31;
    }

    @Override
    public int createRightMember() {
        return random.nextInt(70) + 31;
    }

    @Override
    public void setBcBounty() {
        bcBounty = 3;
    }

}


